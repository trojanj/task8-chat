const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
  create(message) {
    return MessageRepository.create(message);
  }

  getMessages() {
    return MessageRepository.getAll()
  }

  update(id, dataToUpdate) {
    return MessageRepository.update(id, dataToUpdate);
  }

  delete(id) {
    const message = MessageRepository.delete(id);
    if (!message.length) {
      const err = new Error('No message with such id!');
      err.status = 404;
      throw err;
    }
    return message;
  }

  search(search) {
    const message = MessageRepository.getOne(search);
    if (!message) {
      const err = new Error('No message with such id!');
      err.status = 404;
      throw err
    }
    return message;
  }
}

module.exports = new MessageService();
