const {UserRepository} = require('../repositories/userRepository');

class AuthService {
    login(userData) {
        const user = UserRepository.getOne(userData);
        if(!user) {
            const err = new Error('No user with such email');
            err.status = 404;
            throw err;
        }
        return user;
    }

    checkPassword(inputPassword, userPassword) {
        if (inputPassword !== userPassword) {
            const err = new Error('Password is invalid');
            err.status = 401;
            throw err
        }
    }
}

module.exports = new AuthService();
