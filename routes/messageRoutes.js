const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function(req, res, next) {
  try {
    const messages = MessageService.getMessages();
    res.data = messages;
  } catch(err) {
    res.err = err;
  } finally {
    next()
  }
}, responseMiddleware)

router.get('/:id', function(req, res, next) {
  try {
    const id = req.params.id;
    const message = MessageService.search({id});
    res.data = message;
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)

router.post('/', function(req, res, next) {
  try {
    const message = MessageService.create(req.body);
    res.data = message;
  } catch(err) {
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware)

router.put('/:id', function(req, res, next) {
  try {
    const id = req.params.id;
    const dataToUpdate = req.body;
    MessageService.search({id});
    const message = MessageService.update(id, dataToUpdate);
    res.data = message;
  } catch (err) {
    res.err = err;
  } finally {
    next()
  }
}, responseMiddleware)

router.delete('/:id', function(req, res, next) {
  try {
    const id = req.params.id;
    const message = MessageService.delete(id);
    res.data = message;
  } catch (err) {
    res.err = err;
  } finally {
    next()
  }
}, responseMiddleware)

module.exports = router;
