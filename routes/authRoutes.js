const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
	try {
		const email = req.body.email;
		const inputPassword = req.body.password;
		const user = AuthService.login({ email });
		AuthService.checkPassword(inputPassword, user.password);
		res.data = user;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
}, responseMiddleware);

module.exports = router;