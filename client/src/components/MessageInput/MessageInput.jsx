import React, { useState } from 'react';
import classes from './MessageInput.module.css';
import PropTypes from 'prop-types';

export const MessageInput = ({user, sendMessage}) => {
  const [text, setText] = useState('');

  const onChangeHandler = e => {
    const value = e.target.value;

    setText(value);
  }

  const onSendHandler = () => {
    if (!text.trim()) return;

    const newMessage = {
      text,
      avatar: user.avatar,
      user: user.name,
      userId: user.id,
      likes: []
    };
    sendMessage(newMessage);

    setText('');
  }

  return (
    <div className={classes.MessageInput}>
      <textarea
        placeholder='Write something'
        rows={1}
        value={text}
        onChange={onChangeHandler}
      />
      <button
        type='submit'
        onClick={onSendHandler}
      >
        Send
      </button>
    </div>
    )
}

MessageInput.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  sendMessage: PropTypes.func
}
