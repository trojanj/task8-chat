import React from 'react';
import { Button, Card } from 'semantic-ui-react';

import classes from './UserItem.module.css';

export const UserItem = ({user, onEdit, onDelete}) => {
  const {name, surname, email, avatar, id} = user;

  return (
    <Card>
      <Card.Content className={classes.content}>
        <div>
          <h3>{name} {surname}</h3>
          <p>Email: {email}</p>
        </div>
        <div className={classes.img}>
          <img src={avatar} alt="avatar"/>
        </div>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='blue' onClick={() => onEdit(id)}>
            Edit
          </Button>
          <Button basic color='red' onClick={() => onDelete(id)}>
            Delete
          </Button>
        </div>
      </Card.Content>
    </Card>
  )
}

