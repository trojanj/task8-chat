import React from 'react';

import classes from './Header.module.css';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

const Header = ({isAdmin}) => {
  return (
    <div className={classes.Header}>
      <div>My chat</div>
      {isAdmin && (
        <>
          <NavLink exact to='/' activeClassName={classes.active}>
            <span>Chat</span>
          </NavLink>
          <NavLink exact to='/user-list' activeClassName={classes.active}>
            <span>User list</span>
          </NavLink>
          <NavLink exact to='/user-editor' activeClassName={classes.active}>
            <span>Add user</span>
          </NavLink>
        </>
      )}
    </div>
  )
}

const mapStateToProps = state => ({
  isAdmin: state.auth.user?.isAdmin
})

export default connect(mapStateToProps)(Header);
