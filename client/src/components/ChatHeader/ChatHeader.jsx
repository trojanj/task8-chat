import React from 'react';
import { getDateTime, getTextPassedDays, passedDaysFromDate } from '../../helpers';
import PropTypes from 'prop-types';

import classes from './ChatHeader.module.css';

export const ChatHeader = ({usersNumber, messagesNumber, lastMessageCreatedAt, chatName}) => {
  const lastMessageDate = new Date(lastMessageCreatedAt);
  const lastMessageTime = getDateTime(lastMessageDate);
  const passedDays = passedDaysFromDate(lastMessageDate);
  const day = getTextPassedDays(passedDays, lastMessageDate);

  return (
    <div className={classes.ChatHeader}>
      <div>
        <span>{chatName}</span>
        <span>
          {
            usersNumber > 1
              ? `${usersNumber} participants`
              : `${usersNumber} participant`
          }
        </span>
        <span>
          {
            messagesNumber > 1
            ? `${messagesNumber} messages`
            : `${messagesNumber} message`
          }
        </span>
      </div>

      <span>last message {day} at {lastMessageTime}</span>
    </div>
  )
}

ChatHeader.propTypes = {
  participantsNumber: PropTypes.number,
  messagesNumber: PropTypes.number,
  lastMessageCreatedAt: PropTypes.string,
  chatName: PropTypes.string
}
