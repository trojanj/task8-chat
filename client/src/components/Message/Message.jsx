import React from 'react';
import { getDateTime } from '../../helpers';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteMessage, likeMessage } from '../../actions/chatActions';

import classes from './Message.module.css';

const Message = withRouter(({message, userData, deleteMessage, likeMessage, history}) => {
  const {text, avatar, user, createdAt, id, likes, userId} = message;

  const time = getDateTime(new Date(createdAt), true);

  const likesClasses = [classes.like];
  if (likes.includes(userData.id)) {
    likesClasses.push(classes.active)
  }

  const messageClasses = [classes.MessageBlock];
  if (userId === userData.id) {
    messageClasses.push(classes.own);
  }

  const onDelete = () => {
    deleteMessage(id)
  }

  const onEdit = () => {
    history.push(`/message-editor/${id}`);
  }

  const onLike = () => {
    if (userId === userData.id) return;

    const updatedMessage = {...message};

    if (message.likes.includes(userData.id)) {
      updatedMessage.likes = message.likes.filter(userId => userId !== userData.id);
    } else {
      updatedMessage.likes = [...message.likes, userData.id];
    }

    likeMessage(updatedMessage);
  }

  return (
    <div className={messageClasses.join(' ')}>
      {
        userData.id !== userId && (
          <div className={classes.authorBlock}>
            <div className={classes.img}>
              <img src={avatar} alt="avatar"/>
            </div>
            <div className={classes.username}>{user}</div>
          </div>
        )
      }
      <div className={classes.message}>
        <pre>{text}</pre>
        <div className={classes.toolbar}>
          <i
            className="fas fa-edit fa-xs"
            onClick={() => onEdit()}
          />
          <i
            className="fas fa-trash fa-xs"
            onClick={() => onDelete()}
          />
        </div>
        <div className={classes.time}>{time}</div>
        <div className={likesClasses.join(' ')}>
          <div>{likes.length}</div>
          <i
            className="fas fa-thumbs-up fa-lg"
            onClick={onLike}
          />
        </div>
      </div>
    </div>
  )
})

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.any),
  userData: PropTypes.objectOf(PropTypes.any),
  deleteMessage: PropTypes.func,
  setEditMessage: PropTypes.func,
  likeMessage: PropTypes.func
}

const mapDispatchToProps = dispatch => ({
  deleteMessage: id => dispatch(deleteMessage(id)),
  likeMessage: updatedMessage => dispatch(likeMessage(updatedMessage))
})

export default connect(null, mapDispatchToProps)(Message);
