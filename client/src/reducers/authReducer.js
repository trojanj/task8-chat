import {
  AUTH_USER_ERROR,
  AUTH_USER_START,
  AUTH_USER_SUCCESS,
  DROP_ERROR,
  SET_AUTO_LOGIN_LOADING
} from '../constants/actionTypes';

const initialState = {
  user: null,
  isAuthorized: false,
  isLoading: false,
  autoLoginLoading: false,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTH_USER_SUCCESS:
      return {
        ...state,
        user: action.user,
        isAuthorized: true,
        isLoading: false,
        autoLoginLoading: false
      }
    case AUTH_USER_ERROR:
      return {
        ...state,
        error: action.error,
        isLoading: false,
        autoLoginLoading: false
      }
    case AUTH_USER_START:
      return {
        ...state,
        isLoading: true
      }
    case DROP_ERROR:
      return {
        ...state,
        error: null
      }
    case SET_AUTO_LOGIN_LOADING:
      return {
        ...state,
        autoLoginLoading: true
      }
    default:
      return state
  }
}

