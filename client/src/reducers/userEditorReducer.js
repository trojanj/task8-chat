import { DROP_USER, FETCH_USER_ERROR, FETCH_USER_START, FETCH_USER_SUCCESS } from '../constants/actionTypes';

const initialState = {
  user: {},
  isLoading: false,
  error: null
}

const userEditorReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_START:
      return {
        ...state, isLoading: true
      }
    case FETCH_USER_SUCCESS:
      return {
        ...state, user: action.user, isLoading: false
      }
    case FETCH_USER_ERROR:
      return {
        ...state, error: action.error, isLoading: false
      }
    case DROP_USER:
      return initialState
    default:
      return state;
  }
}

export default userEditorReducer;
