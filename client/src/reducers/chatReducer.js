import { FETCH_MESSAGES_ERROR, FETCH_MESSAGES_SUCCESS, SET_CHAT_LOADING } from '../constants/actionTypes';

const initialState = {
  messages: [],
  isLoading: false,
  chatName: 'My chat',
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CHAT_LOADING:
      return {
        ...state, isLoading: true
      }
    case FETCH_MESSAGES_SUCCESS:
      return {
        ...state, messages: action.messages, isLoading: false
      }
    case FETCH_MESSAGES_ERROR:
      return {
        ...state, error: action.error, isLoading: false
      }
    default:
      return state
  }
}
