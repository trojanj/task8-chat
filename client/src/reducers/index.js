import { combineReducers } from 'redux';
import chat from './chatReducer';
import auth from './authReducer';
import messageEditor from './messageEditorReducer';
import users from './usersReducer';
import userEditor from './userEditorReducer';

const rootReducer = combineReducers({
  chat,
  auth,
  messageEditor,
  users,
  userEditor
});

export default rootReducer;
