import { FETCH_USERS_ERROR, FETCH_USERS_SUCCESS, SET_USERS_LOADING } from '../constants/actionTypes';

const initialState = {
  users: [],
  isLoading: false,
  error: null
}

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USERS_LOADING:
      return {
        ...state, isLoading: true
      }
    case FETCH_USERS_SUCCESS:
      return {
        ...state, users: action.users, isLoading: false
      }
    case FETCH_USERS_ERROR:
      return {
        ...state, error: action.error, isLoading: false
      }
    default:
      return state;
  }
}

export default usersReducer
