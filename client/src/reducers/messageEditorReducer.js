import {
  DROP_EDIT_MESSAGE,
  FETCH_MESSAGE_ERROR,
  FETCH_MESSAGE_START,
  FETCH_MESSAGE_SUCCESS,
} from '../constants/actionTypes';

const initialState = {
  message: null,
  isLoading: false,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGE_START:
      return {
        ...state, isLoading: true
      }
    case FETCH_MESSAGE_SUCCESS:
      return {
        ...state, message: action.message, isLoading: false
      }
    case FETCH_MESSAGE_ERROR:
      return {
        ...state, error: action.error, isLoading: false
      }
    case DROP_EDIT_MESSAGE:
      return initialState
    default:
      return state
  }
}
