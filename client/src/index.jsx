import React from 'react';
import { render } from 'react-dom';
import App from './App.jsx';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { BrowserRouter as Router } from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css';
import './index.css';

const store = configureStore();

const app = (
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
)

render(
  app,
  document.getElementById('root')
);

serviceWorker.unregister();
