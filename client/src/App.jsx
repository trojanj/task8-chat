import React, { useEffect } from 'react';
import Chat from './containers/Chat';
import Header from './components/Header';
import Footer from './components/Footer';
import LoginPage from './containers/LoginPage';
import { Redirect, Switch } from 'react-router-dom';
import PublicRoute from './containers/PublicRoute';
import PrivateRoute from './containers/PrivateRoute';
import AdminRoute from './containers/AdminRoute';
import UserList from './containers/UserList'
import { connect } from 'react-redux';
import { autoLogin } from './actions/authActions';
import MessageEditor from './containers/MessageEditor';
import UserPage from './containers/UserPage';
import { Loader } from './components/UI/Loader';

const App = ({autoLogin, autoLoginLoading}) => {
  useEffect(() => {
    autoLogin();
  }, [autoLogin])

  if (autoLoginLoading) {
    return <Loader />
  }

  return (
    <>
      <Header />

      <Switch>
        <PublicRoute exact path='/login' component={LoginPage} />
        <AdminRoute exact path='/user-list' component={UserList} />
        <AdminRoute exact path='/user-editor' component={UserPage} />
        <AdminRoute path='/user-editor/:id' component={UserPage} />
        <PrivateRoute exact path='/' component={Chat}/>
        <PrivateRoute exact path='/message-editor/:id' component={MessageEditor}/>
        <Redirect to='/'/>
      </Switch>

      <Footer />
    </>
  );
}

const mapDispatchToProps = dispatch => ({
  autoLogin: () => dispatch(autoLogin())
})

const mapStateToProps = state => ({
  autoLoginLoading: state.auth.autoLoginLoading
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
