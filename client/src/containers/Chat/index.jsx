import React from 'react';
import { ChatHeader } from '../../components/ChatHeader/ChatHeader';
import { MessageList } from '../../components/MessageList/MessageList';
import { MessageInput } from '../../components/MessageInput/MessageInput';
import { Loader } from '../../components/UI/Loader';
import { connect } from 'react-redux';
import { fetchMessages, sendMessage } from '../../actions/chatActions';
import PropTypes from 'prop-types';

import classes from './Chat.module.css';

class Chat extends React.Component {
  getUsersNumber = () => {
    let users = [];

    this.props.messages.forEach(message => {
      if (!users.includes(message.userId)) {
        users.push(message.userId);
      }
    })

    return users.length;
  }

  handleKeyDown = e => {
    if (e.code === 'ArrowUp') {
      e.preventDefault();

      const lastUserMessage = [...this.props.messages]
        .reverse()
        .find(message => message.userId === this.props.user.id);

      if (!lastUserMessage) return;

      this.props.setEditMessage(lastUserMessage.id, lastUserMessage.text);
    }
  }

  componentDidMount() {
    this.props.fetchMessages();
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  render() {
    if (this.props.isLoading && !this.props.messages.length) {
      return <div className={classes.Chat}>
        <Loader/>
      </div>
    }

    const {messages, chatName, user, sendMessage, isLoading} = this.props;
    const usersNumber = this.getUsersNumber();
    const lastMessageCreatedAt = messages[messages.length - 1]?.createdAt;

    return (
      <div className={classes.Chat}>
        <ChatHeader
          usersNumber={usersNumber}
          messagesNumber={messages.length}
          lastMessageCreatedAt={lastMessageCreatedAt}
          chatName={chatName}
        />
        <MessageList
          messages={messages}
          isLoading={isLoading}
          user={user}
        />
        <MessageInput
          sendMessage={sendMessage}
          user={user}
        />
      </div>
    )
  }
}

Chat.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object),
  isLoading: PropTypes.bool,
  user: PropTypes.objectOf(PropTypes.any),
  chatName: PropTypes.string,
  fetchMessages: PropTypes.func,
  sendMessage: PropTypes.func
}

const mapStateToProps = state => ({
  messages: state.chat.messages,
  isLoading: state.chat.isLoading,
  user: state.auth.user,
  chatName: state.chat.chatName
})

const mapDispatchToProps = dispatch => ({
  fetchMessages: () => dispatch(fetchMessages()),
  sendMessage: newMessage => dispatch(sendMessage(newMessage))
})

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
