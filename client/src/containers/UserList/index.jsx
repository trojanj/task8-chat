import React, { useEffect } from 'react';
import { Card } from 'semantic-ui-react';
import { UserItem } from '../../components/UserItem';
import { connect } from 'react-redux';
import { deleteUser, fetchUsers, setUsersLoading } from '../../actions/usersActions';
import { Loader } from '../../components/UI/Loader';
import PropTypes from 'prop-types';

import classes from './UserList.module.css';

const UserList = ({fetchUsers, deleteUser, setLoading, users, isLoading, history}) => {
  useEffect(() => {
    setLoading();
    fetchUsers();
  }, [setLoading, fetchUsers])

  const onEdit = id => {
    history.push(`/user-editor/${id}`);
  }

  const onDelete = id => {
    deleteUser(id);
  }

  if (isLoading) {
    return <div className={classes.UserList}>
      <Loader />
    </div>
  }

  return (
    <div className={classes.UserList}>
      <Card.Group className={classes.cards}>
        {users.map(user => (
          <UserItem
            key={user.id}
            user={user}
            onEdit={onEdit}
            onDelete={onDelete}
          />
        ))}
      </Card.Group>
    </div>
  )
}

UserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object),
  isLoading: PropTypes.bool,
  error: PropTypes.bool
}

UserList.defaultProps = {
  error: null
}

const mapStateToProps = state => ({
  users: state.users.users,
  isLoading: state.users.isLoading,
  error: state.users.error
})

const mapDispatchToProps = dispatch => ({
  fetchUsers: () => dispatch(fetchUsers()),
  deleteUser: id => dispatch(deleteUser(id)),
  setLoading: () => dispatch(setUsersLoading())
})

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
