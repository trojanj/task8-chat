import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const AdminRoute = ({component: Component, isAuthorized, isAdmin, ...rest}) => (
  <Route
    {...rest}
    render={props => (isAdmin
      ? <Component {...props} />
      : isAuthorized
        ? <Redirect to={{pathname: '/', state: {from: props.location}}}/>
        : <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
    )}
  />
);

AdminRoute.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  isAdmin: PropTypes.bool,
  component: PropTypes.any.isRequired,
  location: PropTypes.any
};

AdminRoute.defaultProps = {
  isAdmin: undefined,
  location: undefined
};

const mapStateToProps = state => ({
  isAuthorized: state.auth.isAuthorized,
  isAdmin: state.auth.user?.isAdmin
});

export default connect(mapStateToProps)(AdminRoute);
