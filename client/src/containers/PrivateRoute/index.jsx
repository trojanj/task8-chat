import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const PrivateRoute = ({ component: Component, isAuthorized, ...rest }) => (
  <Route
    {...rest}
    render={props => (isAuthorized
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
  />
);

PrivateRoute.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  component: PropTypes.any.isRequired,
  location: PropTypes.any
};

PrivateRoute.defaultProps = {
  location: undefined
};

const mapStateToProps = state => ({
  isAuthorized: state.auth.isAuthorized
});

export default connect(mapStateToProps)(PrivateRoute);
