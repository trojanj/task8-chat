import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const PublicRoute = ({ component: Component, isAuthorized, isAdmin, ...rest }) => (
  <Route
    {...rest}
    render={props => (isAdmin
      ? <Redirect to={{ pathname: '/user-list', state: { from: props.location } }} />
      : isAuthorized
        ? <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        : <Component {...props} />)}
  />
);

PublicRoute.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  isAdmin: PropTypes.bool,
  component: PropTypes.any.isRequired,
  location: PropTypes.any
};

PublicRoute.defaultProps = {
  isAdmin: undefined,
  location: undefined
}

const mapStateToProps = state => ({
  isAuthorized: state.auth.isAuthorized,
  isAdmin: state.auth.user?.isAdmin
});

export default connect(mapStateToProps)(PublicRoute);
