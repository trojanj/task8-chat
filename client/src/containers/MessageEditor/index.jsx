import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { dropEditMessage, editMessageDone, fetchMessage } from '../../actions/messageEditorActions';
import { Loader } from '../../components/UI/Loader';

import classes from './MessageEditor.module.css';

const MessageEditor = ({
                         fetchMessage, message, isLoading, editMessageDone,
                         dropEditMessage, match, history
                       }) => {
  const [text, setText] = useState('');
  useEffect(() => {
    fetchMessage(match.params.id);
  }, [fetchMessage, match.params.id])

  useEffect(() => {
    setText(message?.text);
  }, [message])

  const onChangeHandler = e => {
    const value = e.target.value;

    setText(value);
  }

  const onEditDone = () => {
    if (!text.trim()) return;

    const updatedMessage = {...message, text};

    editMessageDone(updatedMessage);

    history.push('/');
    dropEditMessage();
  }

  const onClose = () => {
    dropEditMessage();
    history.push('/');
  }

  const moveCaretAtEnd = e => {
    const value = e.target.value
    e.target.value = ''
    e.target.value = value
  }

  if (isLoading) {
    return <Loader/>
  }

  return (
    <div className={classes.MessageEditor}>
      <div className={classes.form}>
        <h4>Edit message</h4>
        <button
          className={classes.btnClose}
          onClick={onClose}
        >
          <span>&times;</span>
        </button>
        <textarea
          autoFocus
          onFocus={moveCaretAtEnd}
          value={text}
          rows={3}
          onChange={onChangeHandler}
        />
          <div className={classes.buttons}>
            <button
              className={classes.btnCancel}
              onClick={onClose}
            >
              Cancel
            </button>
            <button
              className={classes.btnEdit}
              onClick={onEditDone}
            >
              Save
            </button>
          </div>
      </div>
    </div>
  )
}

MessageEditor.propTypes = {
  text: PropTypes.string,
  editMessageId: PropTypes.string,
  dropEditMessage: PropTypes.func,
  editMessageDone: PropTypes.func
}

const mapStateToProps = state => ({
  message: state.messageEditor.message,
  isLoading: state.messageEditor.isLoading
})

const mapDispatchToProps = dispatch => ({
  fetchMessage: id => dispatch(fetchMessage(id)),
  editMessageDone: updatedMessage => dispatch(editMessageDone(updatedMessage)),
  dropEditMessage: () => dispatch(dropEditMessage())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageEditor);
