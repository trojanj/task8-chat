import React, { useEffect, useState } from 'react';
import { Button, Checkbox, Form } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { dropUser, fetchUser } from '../../actions/userEditorActions';
import { Loader } from '../../components/UI/Loader';
import { addUser, updateUser } from '../../actions/usersActions';
import { DEFAULT_AVATAR } from '../../constants/constants';

import classes from './UserPage.module.css';

const UserPage = ({user, isLoading, fetchUser, updateUser, addUser, dropUser, match, history}) => {
  const [userData, setUserData] = useState({
    name: '',
    surname: '',
    email: '',
    password: '',
    avatar: '',
    isAdmin: false
  });
  const [isShown, setIsShown] = useState(false);

  useEffect(() => {
    if (match.params.id) {
      fetchUser(match.params.id);
    } else if (user.id) {
      dropUser();
    }
  }, [fetchUser, dropUser, match.params.id])

  useEffect(() => {
    if (match.params.id) {
      setUserData({...user});
    } else if (user.id) {
      setUserData({
        name: '',
        surname: '',
        email: '',
        password: '',
        avatar: '',
        isAdmin: false
      });
    }
  }, [user, match.params.id])

  const onChangeData = e => {
    const name = e.target.name;
    const value = e.target.value;
    setUserData(prevState => ({
      ...prevState,
      [name]: value
    }));
  }

  const onCancel = () => {
    history.push('/user-list');
  }

  const onSubmit = () => {
    if (user.id) {
      updateUser(userData);
    } else {
      const newUser = {...userData};
      if (!newUser.avatar) {
        newUser.avatar = DEFAULT_AVATAR;
      }
      addUser(newUser);
    }
   history.push('/user-list');
  }

  const onIsAdminChange = () => {
    setUserData(prevState => ({
      ...prevState,
      isAdmin: !prevState.isAdmin
    }));
  }

  const validateForm = () => {
    if (!userData.name?.trim() || !userData.surname?.trim() || !userData.email?.trim() || !userData.password?.trim()) {
      return true
    } else {
      return false
    }
  }

  if (isLoading) {
    return <div className={classes.UserPage}>
      <Loader/>
    </div>
  }

  const isDisabled = validateForm();

  return (
    <div className={classes.UserPage}>
      <Form>
        <Form.Field required>
          <label>Name</label>
          <input
            placeholder='Name'
            name='name'
            value={userData.name}
            onChange={onChangeData}/>
        </Form.Field>
        <Form.Field required>
          <label>Surname</label>
          <input
            placeholder='Surname'
            name='surname'
            value={userData.surname}
            onChange={onChangeData}/>
        </Form.Field>
        <Form.Field required>
          <label>Email</label>
          <input
            placeholder='Email'
            type='email'
            name='email'
            value={userData.email}
            onChange={onChangeData}/>
        </Form.Field>
        <Form.Field className={classes.password} required>
          <label>Password</label>
          <input
            placeholder='Password'
            type={isShown ? 'text' : 'password'}
            name='password'
            value={userData.password}
            onChange={onChangeData}/>
          <i
            className={isShown ? `fas fa-eye fa-lg ${classes.active}` : 'fas fa-eye fa-lg'}
            onClick={() => setIsShown(!isShown)}/>
        </Form.Field>
        <Form.Field>
          <label>Avatar url</label>
          <input
            placeholder='Avatar url'
            name='avatar'
            value={userData.avatar}
            onChange={onChangeData}/>
        </Form.Field>
        <Form.Field>
          <Checkbox
            label='Administrator privileges'
            checked={userData.isAdmin}
            onChange={onIsAdminChange}/>
        </Form.Field>
        <Button className={classes.btnCancel} onClick={onCancel}>Cancel</Button>
        <Button type='submit' primary onClick={onSubmit} disabled={isDisabled}>
          {user.id ? 'Update' : 'Create'}
        </Button>
      </Form>
    </div>
  )
}

const mapStateToProps = state => ({
  user: state.userEditor.user,
  isLoading: state.userEditor.isLoading
})

const mapDispatchToProps = dispatch => ({
  fetchUser: id => dispatch(fetchUser(id)),
  dropUser: () => dispatch(dropUser()),
  updateUser: user => dispatch(updateUser(user)),
  addUser: user => dispatch(addUser(user))
})

export default connect(mapStateToProps, mapDispatchToProps)(UserPage)
