import React, { useState } from 'react';
import { Button, Form, Message } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { authUser, dropError } from '../../actions/authActions';

import classes from './LoginPage.module.css';

const LoginPage = ({isLoading, error, authUser, dropError}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onEmailChange = value => {
    setEmail(value);
  }

  const onPasswordChange = value => {
    setPassword(value);
  }

  const onLoginClick = () => {
    if (email && password) {
      authUser(email, password);
    }
  }

  const onFocusHandler = () => {
    if (error) {
      dropError();
    }
  }

  return (
    <div className={classes.LoginPage}>
      <Form loading={isLoading} error={!!error}>
        <Form.Field>
          <label>Email</label>
          <input
            placeholder='Email'
            value={email}
            onChange={e => onEmailChange(e.target.value)}
            onFocus={onFocusHandler}
          />
        </Form.Field>
        <Form.Field>
          <label>Password</label>
          <input
            placeholder='Password'
            type='password'
            value={password}
            onChange={e => onPasswordChange(e.target.value)}
            onFocus={onFocusHandler}
          />
        </Form.Field>
        <Message
          error
          header={error?.name}
          content={error?.message}
        />
        <Button
          type='submit'
          onClick={onLoginClick}
        >Log In</Button>
      </Form>
    </div>
  )
}

const mapStateToProps = state => ({
  isLoading: state.auth.isLoading,
  error: state.auth.error
})

const mapDispatchToProps = dispatch => ({
  authUser: (email, password) => dispatch(authUser(email, password)),
  dropError: () => dispatch(dropError())
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
