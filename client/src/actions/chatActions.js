import { DELETE_MESSAGE, FETCH_MESSAGES, LIKE_MESSAGE, SEND_MESSAGE } from '../constants/actionTypes';

export function fetchMessages() {
  return {
    type: FETCH_MESSAGES
  }
}

export function sendMessage(newMessage) {
  return {
    type: SEND_MESSAGE,
    newMessage
  }
}

export function deleteMessage(id) {
  return {
    type: DELETE_MESSAGE,
    id
  }
}

export function likeMessage(updatedMessage) {
  return {
    type: LIKE_MESSAGE,
    updatedMessage
  }
}

