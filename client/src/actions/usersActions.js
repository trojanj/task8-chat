import { ADD_USER, DELETE_USER, FETCH_USERS, SET_USERS_LOADING, UPDATE_USER } from '../constants/actionTypes';

export const fetchUsers = () => ({
  type: FETCH_USERS
})

export const deleteUser = id => ({
  type: DELETE_USER,
  id
})

export const setUsersLoading = () => ({
  type: SET_USERS_LOADING
})

export const updateUser = user => ({
  type: UPDATE_USER,
  user
})

export const addUser = user => ({
  type: ADD_USER,
  user
})
