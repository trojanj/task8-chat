import { DROP_EDIT_MESSAGE, EDIT_MESSAGE_DONE, FETCH_MESSAGE } from '../constants/actionTypes';

export const fetchMessage = id => ({
  type: FETCH_MESSAGE,
  id
})

export const editMessageDone = updatedMessage => ({
  type: EDIT_MESSAGE_DONE,
  updatedMessage
})

export const dropEditMessage = () => ({
  type: DROP_EDIT_MESSAGE
})
