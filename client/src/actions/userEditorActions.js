import { DROP_USER, FETCH_USER } from '../constants/actionTypes';

export const fetchUser = id => ({
  type: FETCH_USER,
  id
})

export const dropUser = () => ({
  type: DROP_USER
})
