import { AUTH_USER, AUTO_LOGIN, DROP_ERROR } from '../constants/actionTypes';

export const authUser = (email, password) => ({
  type: AUTH_USER,
  payload: {
    email,
    password
  }
})

export const dropError = () => ({
  type: DROP_ERROR
})

export const autoLogin = () => ({
  type: AUTO_LOGIN,
})
