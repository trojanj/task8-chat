export const MS_IN_ONE_DAY = 864e5;
export const API_URL = 'http://localhost:3050/api';
export const DEFAULT_AVATAR = process.env.PUBLIC_URL + '/img/avatar.png';
