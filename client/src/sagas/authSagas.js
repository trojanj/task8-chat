import { all, call, put, takeEvery } from 'redux-saga/effects';
import {
  AUTH_USER,
  AUTH_USER_ERROR,
  AUTH_USER_START,
  AUTH_USER_SUCCESS,
  AUTO_LOGIN,
  SET_AUTO_LOGIN_LOADING
} from '../constants/actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/constants';

function* authUser(action) {
  try {
    yield put({type: AUTH_USER_START});

    const credentials = action.payload;
    const response = yield call(axios.post, `${API_URL}/auth/login`, credentials);

    sessionStorage.setItem('credentials', JSON.stringify(credentials));

    yield put({type: AUTH_USER_SUCCESS, user: response.data});
  } catch (error) {
    yield put({ type: AUTH_USER_ERROR, error });
  }
}

function* watchAuthUser() {
  yield takeEvery(AUTH_USER, authUser);
}

function* autoLogin() {
  try {
    const credentials = JSON.parse(sessionStorage.getItem('credentials'));

    if (credentials) {
      yield put({type: SET_AUTO_LOGIN_LOADING});

      const response = yield call(axios.post, `${API_URL}/auth/login`, credentials);

      yield put({type: AUTH_USER_SUCCESS, user: response.data});
    }
  } catch (error) {
    yield put({type: AUTH_USER_ERROR, error})
  }
}

function* watchAutoLogin() {
  yield takeEvery(AUTO_LOGIN, autoLogin);
}

export default function* authSagas() {
  yield all([
    watchAuthUser(),
    watchAutoLogin()
  ])
};
