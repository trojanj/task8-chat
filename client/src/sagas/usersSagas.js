import { all, call, put, takeEvery } from 'redux-saga/effects';
import {
  ADD_USER,
  DELETE_USER,
  FETCH_USERS,
  FETCH_USERS_ERROR,
  FETCH_USERS_SUCCESS,
  SET_USERS_LOADING, UPDATE_USER
} from '../constants/actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/constants';

function* fetchUsers() {
  try {
    const response = yield call(axios.get, `${API_URL}/users`);

    yield put({type: FETCH_USERS_SUCCESS, users: response.data});
  } catch (error) {
    yield put({type: FETCH_USERS_ERROR, error});
  }
}

function* watchFetchUsers() {
  yield takeEvery(FETCH_USERS, fetchUsers);
}

export function* deleteUser(action) {
  try {
    yield put({type: SET_USERS_LOADING});
    yield call(axios.delete, `${API_URL}/users/${action.id}`);
    yield put({type: FETCH_USERS});
  } catch (error) {
    yield put({type: FETCH_USERS_ERROR, error});
  }
}

function* watchDeleteUser() {
  yield takeEvery(DELETE_USER, deleteUser)
}

export function* addUser(action) {
  try {
    yield put({type: SET_USERS_LOADING});
    yield call(axios.post, `${API_URL}/users`, action.user);
    yield put({ type: FETCH_USERS });
  } catch (error) {
    yield put({type: FETCH_USERS_ERROR, error});
  }
}

function* watchAddUser() {
  yield takeEvery(ADD_USER, addUser)
}

export function* updateUser(action) {
  try {
    yield put({type: SET_USERS_LOADING});
    yield call(axios.put, `${API_URL}/user/${action.user.id}`, action.user);
    yield put({ type: FETCH_USERS });
  } catch (error) {
    yield put({type: FETCH_USERS_ERROR, error});
  }
}

function* watchUpdateUser() {
  yield takeEvery(UPDATE_USER, updateUser)
}

export default function* usersSagas() {
  yield all([
    watchFetchUsers(),
    watchDeleteUser(),
    watchAddUser(),
    watchUpdateUser()
  ])
};
