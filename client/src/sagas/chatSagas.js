import { all, call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { API_URL } from '../constants/constants';
import {
  DELETE_MESSAGE,
  FETCH_MESSAGES,
  FETCH_MESSAGES_ERROR,
  FETCH_MESSAGES_SUCCESS,
  LIKE_MESSAGE,
  SEND_MESSAGE,
  SET_CHAT_LOADING
} from '../constants/actionTypes';

function* fetchMessages() {
  try {
    yield put({type: SET_CHAT_LOADING});

    const response = yield call(axios.get, `${API_URL}/messages`);

    yield put({type: FETCH_MESSAGES_SUCCESS, messages: response.data});
  } catch (error) {
    yield put({type: FETCH_MESSAGES_ERROR, error});
  }
}

function* watchFetchMessages() {
  yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

function* deleteMessage(action) {
  try {
    yield put({type: SET_CHAT_LOADING});

    yield call(axios.delete, `${API_URL}/messages/${action.id}`);

    yield put({type: FETCH_MESSAGES});
  } catch (error) {
    console.log(error)
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

function* sendMessage(action) {
  try {
    yield put({type: SET_CHAT_LOADING});

    yield call(axios.post, `${API_URL}/messages/`, action.newMessage);

    yield put({type: FETCH_MESSAGES});
  } catch (error) {
    console.log(error);
  }
}

function* watchSendMessage() {
  yield takeEvery(SEND_MESSAGE, sendMessage);
}

function* likeMessage(action) {
  try {
    yield put({type: SET_CHAT_LOADING});
    yield call(axios.put, `${API_URL}/messages/${action.updatedMessage.id}`, action.updatedMessage);

    yield put({type: FETCH_MESSAGES});
  } catch (error) {
    console.log(error);
  }
}

function* watchLikeMessage() {
  yield takeEvery(LIKE_MESSAGE, likeMessage);
}

export default function* chatSagas() {
  yield all([
    watchFetchMessages(),
    watchDeleteMessage(),
    watchSendMessage(),
    watchLikeMessage()
  ])
};
