import axios from 'axios';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { API_URL } from '../constants/constants';
import { FETCH_USER, FETCH_USER_ERROR, FETCH_USER_START, FETCH_USER_SUCCESS } from '../constants/actionTypes';

function* fetchUser(action) {
  try {
    yield put({type: FETCH_USER_START});
    const user = yield call(axios.get, `${API_URL}/users/${action.id}`);
    yield put({type: FETCH_USER_SUCCESS, user: user.data})
  } catch (error) {
    yield put({type: FETCH_USER_ERROR, error})
  }
}

function* watchFetchUser() {
  yield takeEvery(FETCH_USER, fetchUser);
}

export default function* userEditorSagas() {
  yield all([
    watchFetchUser()
  ])
};
