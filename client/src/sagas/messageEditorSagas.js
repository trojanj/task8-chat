import { all, call, put, takeEvery } from 'redux-saga/effects';
import {
  EDIT_MESSAGE_DONE,
  FETCH_MESSAGE,
  FETCH_MESSAGE_ERROR,
  FETCH_MESSAGE_START,
  FETCH_MESSAGE_SUCCESS,
  FETCH_MESSAGES,
  SET_CHAT_LOADING
} from '../constants/actionTypes';
import axios from 'axios';
import { API_URL } from '../constants/constants';

function* fetchMessage(action) {
  try {
    yield put({type: FETCH_MESSAGE_START});

    const response = yield call(axios.get, `${API_URL}/messages/${action.id}`);

    yield put({type: FETCH_MESSAGE_SUCCESS, message: response.data});
  } catch (error) {
    yield put({type: FETCH_MESSAGE_ERROR, error});
  }
}

function* watchFetchMessage() {
  yield takeEvery(FETCH_MESSAGE, fetchMessage);
}

function* editMessageDone(action) {
  try {
    yield put({type: SET_CHAT_LOADING});

    yield call(axios.put, `${API_URL}/messages/${action.updatedMessage.id}`, action.updatedMessage);

    yield put({type: FETCH_MESSAGES});
  } catch (error) {
    console.log(error);
  }
}

function* watchEditMessageDone() {
  yield takeEvery(EDIT_MESSAGE_DONE, editMessageDone);
}

export default function* messageEditorSagas() {
  yield all([
    watchFetchMessage(),
    watchEditMessageDone()
  ])
};
