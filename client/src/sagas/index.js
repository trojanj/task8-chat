import { all } from 'redux-saga/effects';
import authSagas from './authSagas';
import chatSagas from './chatSagas';
import messageEditorSagas from './messageEditorSagas';
import usersSagas from './usersSagas';
import userEditorSagas from './userEditorSagas';

export default function* rootSaga() {
  yield all([
    authSagas(),
    chatSagas(),
    messageEditorSagas(),
    usersSagas(),
    userEditorSagas()
  ])
};
