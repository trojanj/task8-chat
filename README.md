## Installation

1. `git clone https://trojanj@bitbucket.org/trojanj/task8-chat.git`
2. `cd task8-chat`
3. `npm run client-install`
4. `npm run server-install`
5. `npm start`

## Login
1. Admin: login - admin@mail.com, password - admin
2. User: login - user@mail.com, password - user
